package ioc.m08.eac3.advanced_notes.activitats;

import ioc.m08.eac3.advanced_notes.R;
import ioc.m08.eac3.advanced_notes.dades.Nota;
import ioc.m08.eac3.advanced_notes.database.DBNotes;
import ioc.m08.eac3.advanced_notes.fragments.FragmentEditNota;
import ioc.m08.eac3.advanced_notes.fragments.FragmentEditNotaImatge;
import ioc.m08.eac3.advanced_notes.fragments.FragmentEditNotaLloc;
import ioc.m08.eac3.advanced_notes.fragments.FragmentEditNotaSo;
import ioc.m08.eac3.advanced_notes.fragments.FragmentEditNotaText;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

/**
 * Aquesta activitat permet visualitzar i eliminar les notes.
 * 
 * @author Javier Garc�a
 * 
 */
public class EditNotaActivity extends FragmentActivity implements OnClickListener {
	private static final String TAG = "EditNotaActivity";

	// Fragments
	FragmentManager mFragmentManager;
	FragmentTransaction mFragmentTransaction;
	Fragment mFragment;
	FragmentEditNota mFragmentAction;

	// Dades nota
	int mNotaId;
	int mNotaTipus;
	String mNotaTitol;

	// UI
	Button mBotoEsborrar;

	// Base de dades
	DBNotes db = new DBNotes(this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_nota);

		loadFragment();
		initWidgets();
	}

	/**
	 * Inicialitza els widgets.
	 */
	private void initWidgets() {
		mBotoEsborrar = (Button) findViewById(R.id.buttonEsborrar);
		mBotoEsborrar.setOnClickListener(this);
	}

	/**
	 * Carregar el fragment apropiat pel tipus de nota visualitzada.
	 */
	private void loadFragment() {
		// Iniciem la transacci�
		mFragmentManager = getSupportFragmentManager();
		mFragmentTransaction = mFragmentManager.beginTransaction();

		// Obtenim les dades de la nota
		Intent intent = getIntent();
		mNotaId = intent.getIntExtra("_id", -1);
		mNotaTipus = intent.getIntExtra("tipus", -1);
		mNotaTitol = intent.getStringExtra("titol");

		//
		// Obtenim el fragment apropiat
		switch (mNotaTipus) {
		case Nota.TEXT:
			mFragment = new FragmentEditNotaText();
			break;

		case Nota.LLOC:
			mFragment = new FragmentEditNotaLloc();
			break;

		case Nota.IMATGE:
			mFragment = new FragmentEditNotaImatge();
			break;

		case Nota.SO:
			mFragment = new FragmentEditNotaSo();
			break;
		}

		// Passem els arguments al fragment
		Bundle arguments = new Bundle();
		arguments.putInt("_id", mNotaId);
		mFragment.setArguments(arguments);

		// Reemplacem el fragment
		mFragmentTransaction.replace(R.id.frameLayoutContainer, mFragment);
		mFragmentTransaction.commit();

		// Guardem el fragment per poder realitzar accions sobre ells
		mFragmentAction = (FragmentEditNota) mFragment;
	}

	/**
	 * Al clicar el bot� esborrem la nota de la base de dades i del dispositiu
	 * segons el tipus de nota si el resultat es correcte.
	 */
	@Override
	public void onClick(View v) {
		if (v == mBotoEsborrar) {
			boolean correcte = db.open().deleteNotaById(mNotaId);
			if (correcte) {
				Toast.makeText(this, getResources().getString(R.string.nota_esborrada),
						Toast.LENGTH_SHORT).show();

				// Realitza la acci� especifica de neteja del fragment
				mFragmentAction.removeNota();

				// Finalitzem la activitat
				finish();
			}
		}
	}
}
