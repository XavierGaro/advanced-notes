package ioc.m08.eac3.advanced_notes.activitats;

import ioc.m08.eac3.advanced_notes.R;
import ioc.m08.eac3.advanced_notes.dades.Nota;
import ioc.m08.eac3.advanced_notes.database.DBNotes;
import ioc.m08.eac3.advanced_notes.fragments.FragmentAddNota;
import ioc.m08.eac3.advanced_notes.fragments.FragmentAddNotaImatge;
import ioc.m08.eac3.advanced_notes.fragments.FragmentAddNotaLloc;
import ioc.m08.eac3.advanced_notes.fragments.FragmentAddNotaSo;
import ioc.m08.eac3.advanced_notes.fragments.FragmentAddNotaText;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

/**
 * Aquesta activitat permet afegir noves notes a la aplicaci�.
 * 
 * @author Javier Garc�a
 * 
 */
public class AddNotaActivity extends FragmentActivity implements OnClickListener,
		TemporalFileManager {
	private static final String TAG = "AddNotaActivity";

	// UI
	private ImageButton mBotoText;
	private ImageButton mBotoImatge;
	private ImageButton mBotoLloc;
	private ImageButton mBotoSo;
	private Button mBotoAfegir;
	private EditText mEditTextTitol;

	// Fragments
	FragmentManager mFragmentManager;
	FragmentTransaction mFragmentTransaction;
	Fragment mFragment;
	FragmentAddNota mFragmentAction;

	// Base de dades
	DBNotes mDatabase = new DBNotes(this);

	// Dades de la nota actual
	private int mTipusActual;
	private List<File> arxiusTemporals = new ArrayList<File>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_nota);

		// Establim el tipus de nota per defecte
		mTipusActual = Nota.TEXT;

		// Carreguem el fragment
		loadFragment();

		// Inicialitzem els botons
		initWidgets();
	}

	/**
	 * Carregar el fragment apropiat pel tipus de nota seleccionada.
	 */
	private void loadFragment() {
		// Iniciem la transacci�
		mFragmentManager = getSupportFragmentManager();
		mFragmentTransaction = mFragmentManager.beginTransaction();

		// Obtenim el fragment apropiat
		switch (mTipusActual) {
		case Nota.TEXT:
			mFragment = new FragmentAddNotaText();
			break;

		case Nota.IMATGE:
			mFragment = new FragmentAddNotaImatge();
			break;

		case Nota.LLOC:
			mFragment = new FragmentAddNotaLloc();
			break;

		case Nota.SO:
			mFragment = new FragmentAddNotaSo();
			break;
		}

		// Reemplacem el fragment i ho confirmem.
		mFragmentTransaction.replace(R.id.frameLayoutContainer, mFragment);
		mFragmentTransaction.commit();

		// Guardem el fragment per poder realitzar accions sobre ells
		mFragmentAction = (FragmentAddNota) mFragment;

	}

	/**
	 * Inicialitza els widgets.
	 */
	private void initWidgets() {
		mBotoText = (ImageButton) findViewById(R.id.imageButtonText);
		mBotoText.setOnClickListener(this);

		mBotoImatge = (ImageButton) findViewById(R.id.imageButtonImatge);
		mBotoImatge.setOnClickListener(this);

		mBotoLloc = (ImageButton) findViewById(R.id.imageButtonLloc);
		mBotoLloc.setOnClickListener(this);

		mBotoSo = (ImageButton) findViewById(R.id.imageButtonSo);
		mBotoSo.setOnClickListener(this);

		mBotoAfegir = (Button) findViewById(R.id.buttonAfegir);
		mBotoAfegir.setOnClickListener(this);

		mEditTextTitol = (EditText) findViewById(R.id.editTextTitol);
	}

	/**
	 * Comprovem si el bot� clicat es diferent de la selecci� actual i si es
	 * aix� canviem la selecci� i carreguem el fragment, o afegeix les dades de
	 * la nota actual.
	 */
	@Override
	public void onClick(View v) {
		int tipusActual = mTipusActual;

		if (v == mBotoText && mTipusActual != Nota.TEXT) {
			mTipusActual = Nota.TEXT;

		} else if (v == mBotoImatge && mTipusActual != Nota.IMATGE) {
			mTipusActual = Nota.IMATGE;

		} else if (v == mBotoLloc && mTipusActual != Nota.LLOC) {
			// Carregar framgent de lloc
			removeMapFragment();
			mTipusActual = Nota.LLOC;

		} else if (v == mBotoSo && mTipusActual != Nota.SO) {
			// Carregar fragment de so
			mTipusActual = Nota.SO;

		} else if (v == mBotoAfegir) {
			// Afegeix les dades
			addNote();
		}

		// Si ha canviat el tipus actual, carreguem el fragment
		if (tipusActual != mTipusActual)
			loadFragment();
	}

	/**
	 * Ens assegurem que s'ha eliminat el fragment del mapa abans de tornar a
	 * obrir-lo. Aix� evita que el programa falli si canviem de tipus de nota i
	 * tornem a intentar a obrir la de Lloc.
	 */
	private void removeMapFragment() {
		Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.map);

		// Si s'ha trobat el fragment l'eliminem
		if (fragment != null) {
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			ft.remove(fragment);
			ft.commit();
		}
	}

	/**
	 * Comprova que les dades siguin correctes per afegir-es a la base de dades,
	 * si es correcte l'afegeix i retorna a la activitat principal.
	 * 
	 */
	private void addNote() {
		// Construim la nota amb les dades del fragment
		Nota nota = mFragmentAction.getDades(mEditTextTitol.getText().toString());

		// Si no s'ha pogut crear la nota mostrem el missatge d'error
		if (nota == null) {
			Toast.makeText(this, R.string.error_nota, Toast.LENGTH_SHORT).show();
			return;
		}

		// Si s'ha pogut crear eliminem els arxius temporals
		removeTemporal();

		// Inserim la nota a la base de dades
		mDatabase.insertNota(nota);

		Toast.makeText(this, getResources().getString(R.string.nota_afegida), Toast.LENGTH_SHORT)
				.show();

		// Retornem a la aplicaci� principal
		finish();
	}

	/**
	 * Esborra els arxius de la llista temporal
	 */
	private void removeTemporal() {
		for (File file : arxiusTemporals) {
			file.delete();
		}
		arxiusTemporals.clear();
	}

	@Override
	public void onPause() {
		super.onPause();
		// Al parar l'aplicaci� s'eliminen tots els arxius temporals
		removeTemporal();
	}

	@Override
	public void removeArxiusTemporals(File file) {
		if (arxiusTemporals.contains(file)) {
			arxiusTemporals.remove(file);
		}
	}

	@Override
	public void addArxiusTemporals(File file) {
		arxiusTemporals.add(file);
	}
}
