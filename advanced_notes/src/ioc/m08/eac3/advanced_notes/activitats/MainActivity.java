package ioc.m08.eac3.advanced_notes.activitats;

import ioc.m08.eac3.advanced_notes.R;
import ioc.m08.eac3.advanced_notes.dades.Nota;
import ioc.m08.eac3.advanced_notes.database.CustomCursorAdapter;
import ioc.m08.eac3.advanced_notes.database.DBNotes;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

/**
 * Classe principal de la aplicaci� que mostra la llista de notes enregistredes
 * i la opci� de crear-ne de noves. Al seleccionar una de la llista s'obre la
 * activitat que permet inspercionar-les i esborrar-les.
 * 
 * @author Javier Garc�a
 * 
 */
public class MainActivity extends Activity implements OnMenuItemClickListener, OnItemClickListener {
	private static final String TAG = "MainActivity";

	// Notes
	public static final int TIPUS_DE_NOTES = 4;
	public static final int[] ICONES = new int[TIPUS_DE_NOTES];

	// Directoris
	public static final String FOLDER_MULTIMEDIA = "/advanced_notes";
	public static final String FOLDER_SO = FOLDER_MULTIMEDIA + "/so";
	public static final String FOLDER_IMATGE = FOLDER_MULTIMEDIA + "/fotos";

	// Base de dades
	DBNotes mDataBase = new DBNotes(this);

	// UI
	MenuItem mMenuItemAddNota;
	ListView mListViewNotes;
	CursorAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initWidgets();
	}

	/**
	 * Inicialitza els widgets.
	 */
	private void initWidgets() {
		// Assignem la id a les icones
		ICONES[Nota.TEXT] = R.drawable.ic_action_copy;
		ICONES[Nota.IMATGE] = R.drawable.ic_action_camera;
		ICONES[Nota.LLOC] = R.drawable.ic_action_location_found;
		ICONES[Nota.SO] = R.drawable.ic_action_mic;

		// Inicialitzem la llista i l'adaptador
		mAdapter = new CustomCursorAdapter(this, mDataBase.getAllNotes(), 0);
		mListViewNotes = (ListView) findViewById(R.id.listViewNotes);
		mListViewNotes.setAdapter(mAdapter);
		mListViewNotes.setOnItemClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_nota, menu);

		// Enregistrem la acci� al men�
		mMenuItemAddNota = menu.findItem(R.id.action_add_nota);
		mMenuItemAddNota.setOnMenuItemClickListener(this);

		return true;
	}

	@Override
	public boolean onMenuItemClick(MenuItem mi) {
		if (mi == mMenuItemAddNota) {
			// Llencem la activitat per afegir nota i consumim l'event
			Intent intent = new Intent(MainActivity.this, AddNotaActivity.class);
			startActivity(intent);
			return true;
		}
		return false;
	}

	/**
	 * Al reanudar la activitat actualitzem el adaptador.
	 */
	@Override
	public void onResume() {
		super.onResume();
		mAdapter.changeCursor(mDataBase.getAllNotes());
	}

	/**
	 * Al clicar un element de la llista llencem l'activitat per veure-le amb la
	 * informaci� necessaria per recuperar la nota completa.
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Cursor cursor = (Cursor) mAdapter.getItem(position);
		cursor.moveToPosition(position);

		Intent intent = new Intent(MainActivity.this, EditNotaActivity.class);
		Bundle bundle = new Bundle();
		bundle.putInt("_id", cursor.getInt(cursor.getColumnIndexOrThrow("_id")));
		bundle.putInt("tipus", cursor.getInt(cursor.getColumnIndexOrThrow("tipus")));
		bundle.putString("titol", cursor.getString(cursor.getColumnIndexOrThrow("titol")));
		intent.putExtras(bundle);

		startActivity(intent);
	}
}
