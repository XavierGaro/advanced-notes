package ioc.m08.eac3.advanced_notes.activitats;

import java.io.File;

public interface TemporalFileManager {

	/**
	 * Elimina l'arxiu de la llista d'arxius temporals.
	 * 
	 * @param file
	 *            arxiu a eliminar de la llista.
	 */
	public void removeArxiusTemporals(File file);

	/**
	 * Afegeix l'arxiu a la llista d'arxius temporals.
	 * 
	 * @param file
	 *            arxiu a afegir a la lista.
	 */
	public void addArxiusTemporals(File file);
}
