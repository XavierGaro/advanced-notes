package ioc.m08.eac3.advanced_notes.location;

import android.location.Location;

public interface OnLocationUpdateListener {

	/**
	 * Aquest m�tode es cridat per altres m�todes per comunicar-nos que hi ha
	 * hagut una acutalitzaci� en les localitzacions.
	 * 
	 * @param location
	 *            localitzaci� actualitzada.
	 */
	void onLocationUpdate(Location location);

}
