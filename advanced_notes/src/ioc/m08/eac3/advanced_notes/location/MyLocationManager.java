package ioc.m08.eac3.advanced_notes.location;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

/**
 * Aquesta classe gestiona les localitzacions. Permet enregistrar-se al
 * listeners GPS i Network, i rebre la ultima localitzaci� coneguda m�s acurada.
 * 
 * @author Javier Garc�a
 * 
 */
public class MyLocationManager {
	private static final String TAG = "MyLocationManager";

	private static final long ONE_MIN = 1000 * 60;
	private static final long TWO_MIN = ONE_MIN * 2;
	private static final long FIVE_MIN = ONE_MIN * 5;
	private static final long MEASURE_TIME = 1000 * 30;
	private static final long POLLING_FREQ = 1000 * 10;
	private static final float MIN_LAST_READ_ACCURACY = 500.0f;
	private static final float MIN_DISTANCE = 10.0f;

	// Localitzaci�
	private LocationManager mLocationManager;
	private LocationListener mLocationListener;
	private Location mBestReading;
	private OnLocationUpdateListener mCallback;

	private Context mContext;

	/**
	 * Requereix un objecte al que comunicar els canvis i un context.
	 * 
	 * @param callback
	 *            objecte al que comunicar els canvis.
	 * @param context
	 *            context
	 */
	public MyLocationManager(OnLocationUpdateListener callback, Context context) {
		this.mCallback = callback;
		this.mContext = context;

		// El listener comunica al callback quan hi ha una actualitzaci� de
		// localitzaci�.
		this.mLocationListener = new LocationListener() {

			@Override
			public void onLocationChanged(Location location) {
				Log.d(TAG, "Location changed");
				// Cridat quan una nova localitzaci� es trobada pel prove�dor
				mCallback.onLocationUpdate(location);
			}

			@Override
			public void onProviderDisabled(String provider) {
				Log.d(TAG, "Provider disabled:" + provider);
			}

			@Override
			public void onProviderEnabled(String provider) {
				Log.d(TAG, "Provider enabled: " + provider);

			}

			@Override
			public void onStatusChanged(String provider, int arg1, Bundle arg2) {
				Log.d(TAG, "Status changed: " + provider);
			}
		};

		initLocationManager();
	}

	/**
	 * Recorre tots els proveidors buscant la millor localitzaci� estimada entre
	 * les ultimes conegudes.
	 * <p>
	 * En el cas de que la �ltima coneguda sigui m�s antiga que 5 minuts
	 * retornarem aquesta localitzaci� per� ens enregistrarem al listener de
	 * manera que la localitzaci� s'actualitzar� quan rebem la localitzaci�
	 * actual.
	 * <p>
	 * Si la antiguitat de la localitzaci� es superior a dos minuts, es guarda
	 * la m�s precisa, si es menor es guarda la m�s recent.
	 * 
	 * @return ultima posici� coneguda o null si no hi ha cap
	 */
	public Location getLastKnownLocation() {
		List<String> matchingProviders = mLocationManager.getAllProviders();
		float bestAccuracy = Float.MAX_VALUE;
		long bestTime = Long.MAX_VALUE;
		Location bestResult = null;

		// Recorrem tots els prove�dors
		for (String provider : matchingProviders) {

			// Obtenim la �ltima localitzaci� coneguda pel prove�dor
			Location location = mLocationManager.getLastKnownLocation(provider);

			// Si hi ha alguna comprovem si es m�s acurada que la actual
			if (location != null) {
				float accuracy = location.getAccuracy();
				long time = System.currentTimeMillis() - location.getTime();

				if (time < TWO_MIN && accuracy < bestAccuracy) {
					// Han passat menys de dos minuts i aquesta localitzaci� es
					// la m�s precisa
					bestResult = location;
					bestAccuracy = accuracy;
					bestTime = time;
				} else if (time > TWO_MIN && bestAccuracy == Float.MAX_VALUE && time < bestTime) {
					// Han passat m�s de dos minuts, aquesta localitzaci� es la
					// m�s recent, i no s'ha trobat cap amb m�nys de dos minuts
					bestResult = location;
					bestTime = time;
				}
			}
		}

		if (bestTime > FIVE_MIN) {
			// Han passat m�s de cinc minuts, ens enregistrem per intentar
			// refrescar la localitzaci�
			register();
		}

		return bestResult;
	}

	/**
	 * Inicalitza el LocationManager i refresca la localitzaci� al Callback.
	 */
	public void initLocationManager() {
		// Acquire reference to the LocationManager
		mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

		// Si no hi ha cap Location Manager disponible retornem

		if (mLocationManager == null)
			return;

		// Obtenim la ultima localitzaci� coneguda
		mBestReading = getLastKnownLocation();

		// Si s'ha trobat la comuniquem al callback
		if (mBestReading != null)
			mCallback.onLocationUpdate(mBestReading);
	}

	/**
	 * Comprova si la �ltima localitzaci� guardada es m�s recent de dos minuts i
	 * te la precisi� m�nima acceptable. Si no es aix� comen�a a escoltar el
	 * canvis de localitzaci� durant 30 segons com a m�xim.
	 */
	public void register() {
		if (mLocationManager == null) {
			Log.w(TAG, "No hi ha cap Location Manager disponible");
			return;
		}

		// Determinem si la localitzaci� actual es prou bona
		if (mBestReading == null
				|| (mBestReading.getAccuracy() > MIN_LAST_READ_ACCURACY || mBestReading.getTime() < System
						.currentTimeMillis() - TWO_MIN)) {

			// Enregistrem l'actualitzaci� de localitzaci� per xarxa
			mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, POLLING_FREQ,
					MIN_DISTANCE, mLocationListener);

			// Enregistrem l'actualitzaci� de localitzaci� per GPS
			mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, POLLING_FREQ,
					MIN_DISTANCE, mLocationListener);

			// Al arribar al limit del temps per mesurar desenregistrem tots els
			// listeners
			Executors.newScheduledThreadPool(1).schedule(new Runnable() {

				@Override
				public void run() {
					unregister();
				}
			}, MEASURE_TIME, TimeUnit.MILLISECONDS);
		}
	}

	/**
	 * Desenregistrar tots els listener de localitzaci�.
	 */
	public void unregister() {
		// Si no hi ha cap Location Manager disponible no fem res
		if (mLocationManager == null) 
			return;

		mLocationManager.removeUpdates(mLocationListener);
	}
}
