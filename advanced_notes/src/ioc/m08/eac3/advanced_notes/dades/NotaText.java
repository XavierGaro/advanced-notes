package ioc.m08.eac3.advanced_notes.dades;

/**
 * Aquesta classe permet emmagatzemar una nota de text.
 * 
 * @author Javier Garc�a
 * 
 */
public class NotaText extends Nota {
	public final String descripcio;

	public NotaText(String titol, String descripcio) {
		super(Nota.TEXT, titol);
		this.descripcio = descripcio;
	}

	/**
	 * Aquest constructor es fet servir per altres classes que extenen NotaText.
	 * 
	 * @param tipus
	 *            tipus de nota.
	 * @param titol
	 *            t�tol de la nota.
	 * @param descripcio
	 *            contingut de la nota.
	 */
	public NotaText(int tipus, String titol, String descripcio) {
		super(tipus, titol);
		this.descripcio = descripcio;
	}
}
