package ioc.m08.eac3.advanced_notes.dades;

/**
 * Classe general per emmagatzemar notes. Totes les propietats son immutables.
 * 
 * @author Javier Garc�a
 * 
 */
public class Nota {
	// Tipus de notes
	public final static int TEXT = 0;
	public final static int IMATGE = 1;
	public final static int LLOC = 2;
	public final static int SO = 3;
	public final static int MAX_TIPUS = 3;

	public final int tipus;
	public final String titol;

	/**
	 * Si no s'especifica un tipus de nota v�lid es llen�a una excepci�.
	 * 
	 * @param tipus
	 *            tipus de nota.
	 * @param titol
	 *            t�tol de la nota.
	 * @throws IllegalArgumentException
	 *             si no es un tipus de nota v�lid.
	 */
	public Nota(int tipus, String titol) {
		if (tipus < 0 || tipus > MAX_TIPUS)
			throw new IllegalArgumentException("No existeix cap tipus amb aquesta id.");
		this.tipus = tipus;
		this.titol = titol;
	}
}
