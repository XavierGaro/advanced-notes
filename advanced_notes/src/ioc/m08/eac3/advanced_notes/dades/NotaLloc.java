package ioc.m08.eac3.advanced_notes.dades;

/**
 * Nota per emmagatzema la informaci� de llocs.
 * 
 * @author Javier Garc�a
 * 
 */
public class NotaLloc extends NotaText {
	public final double latitut;
	public final double longitut;

	/**
	 * S'emmagatzema el titol, la descripci�, la latitut i la longitut del lloc
	 * 
	 * @param titol
	 *            t�tol de la nota
	 * @param descripcio
	 *            contingut de la nota
	 * @param latitut
	 *            latitud del lloc
	 * @param longitut
	 *            longitud del lloc
	 */
	public NotaLloc(String titol, String descripcio, double latitut, double longitut) {
		super(Nota.LLOC, titol, descripcio);
		this.latitut = latitut;
		this.longitut = longitut;
	}
}
