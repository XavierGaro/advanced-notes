package ioc.m08.eac3.advanced_notes.dades;

/**
 * Classe per emmagatzemar tipus de nota multimedia.
 * 
 * @author Javier Garc�a
 * 
 */
public class NotaMultimedia extends Nota {
	public final String nomFitxer;

	/**
	 * Si el tipus de nota no correspon als tipus multimedia llen�a una
	 * excepci�.
	 * 
	 * @param tipus
	 *            tipus de nota
	 * @param titol
	 *            titol de la nota
	 * @param nomFitxer
	 *            nom del fitxer on est� emmagatzemat el contingut multimedia.
	 * @throws IllegalArgumentException
	 *             si s'intenta passar un tipus que no sigui multimedia.
	 */
	public NotaMultimedia(int tipus, String titol, String nomFitxer) {
		super(tipus, titol);
		if (tipus != Nota.IMATGE && tipus != Nota.SO)
			throw new IllegalArgumentException("No es un tipus multimedia.");
		this.nomFitxer = nomFitxer;
	}
}
