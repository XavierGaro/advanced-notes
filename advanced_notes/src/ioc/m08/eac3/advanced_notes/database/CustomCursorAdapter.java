package ioc.m08.eac3.advanced_notes.database;

import ioc.m08.eac3.advanced_notes.R;
import ioc.m08.eac3.advanced_notes.activitats.MainActivity;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Aquesta classe est�n Cursor Adapter per obtenir les dades d'un cursor amb
 * notes generals.
 * 
 * @author Javier Garc�a
 * 
 */
public class CustomCursorAdapter extends CursorAdapter {
	private static final String TAG = "CustomCursorAdapter";


	public CustomCursorAdapter(Context context, Cursor c, int flags) {
		super(context, c, flags);
	}

	/**
	 * Omplim la vista amb les dades del cursor
	 */
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		// Obtenim la id del recurs de la icona
		int iconaId = MainActivity.ICONES[cursor.getInt(cursor.getColumnIndex(DBNotes.CLAU_TIPUS))];

		// Establim la icone al ImageView
		ImageView icona = (ImageView) view.findViewById(R.id.icona);
		icona.setImageResource(iconaId);

		// Afegim les dades al t�tol
		TextView titol = (TextView) view.findViewById(R.id.titol);
		titol.setText(cursor.getString(cursor.getColumnIndex(DBNotes.CLAU_TITOL)));
	}
	
	/**
	 * Retorna la vista de la fila inflada.
	 */
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		return mInflater.inflate(R.layout.list_view_notes, parent, false);
	}
}
