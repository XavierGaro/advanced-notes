package ioc.m08.eac3.advanced_notes.database;

import ioc.m08.eac3.advanced_notes.R;
import ioc.m08.eac3.advanced_notes.activitats.MainActivity;
import ioc.m08.eac3.advanced_notes.dades.Nota;
import ioc.m08.eac3.advanced_notes.dades.NotaLloc;
import ioc.m08.eac3.advanced_notes.dades.NotaMultimedia;
import ioc.m08.eac3.advanced_notes.dades.NotaText;
import ioc.m08.eac3.advanced_notes.utilitats.UtilitatsDisc;

import java.io.File;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.os.Environment;
import android.util.Log;

/**
 * Aquesta classe gestiona la base de dates de la aplicaci� notes. No cal obrir
 * i tancar la base de dades manualment, s'obre autom�ticament.
 * 
 * @author Javier Garc�a
 */
public class DBNotes {
	public static final String TAG = "DBNotes";

	// Base de dades y taula
	public static final String BD_NOM = "BDNotes";
	public static final String BD_TAULA_NOTES = "notes";
	public static final String BD_TAULA_NOTES_TEXT = "notes_text";
	public static final String BD_TAULA_NOTES_LLOC = "notes_lloc";
	public static final String BD_TAULA_NOTES_MULTIMEDIA = "notes_multimedia";

	public static final int VERSIO = 1;

	// Camps
	public static final String CLAU_ID = "_id";
	public static final String CLAU_TIPUS = "tipus";
	public static final String CLAU_TITOL = "titol";
	public static final String CLAU_DESCRIPCIO = "descripcio";
	public static final String CLAU_LATITUD = "latitud";
	public static final String CLAU_LONGITUD = "longitud";
	public static final String CLAU_NOM_FITXER = "nom_fitxer";

	// Consultes per crear les taules
	public static final String BD_CREATE_NOTES = "CREATE TABLE " + BD_TAULA_NOTES + "(" + CLAU_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + CLAU_TIPUS + " INTEGER, " + CLAU_TITOL
			+ " TEXT NOT NULL);";

	public static final String BD_CREATE_NOTES_TEXT = "CREATE TABLE " + BD_TAULA_NOTES_TEXT + "("
			+ CLAU_ID + " INTEGER PRIMARY KEY, " + CLAU_DESCRIPCIO
			+ " TEXT NOT NULL, FOREIGN KEY (" + CLAU_ID + ") REFERENCES " + BD_TAULA_NOTES + "("
			+ CLAU_ID + ") ON DELETE CASCADE);";

	public static final String BD_CREATE_NOTES_LLOC = "CREATE TABLE " + BD_TAULA_NOTES_LLOC + "("
			+ CLAU_ID + " INTEGER PRIMARY KEY, " + CLAU_LATITUD + " REAL," + CLAU_LONGITUD
			+ " REAL, FOREIGN KEY (" + CLAU_ID + ") REFERENCES " + BD_TAULA_NOTES + "(" + CLAU_ID
			+ ") ON DELETE CASCADE);";

	public static final String BD_CREATE_NOTES_MULTIMEDIA = "CREATE TABLE "
			+ BD_TAULA_NOTES_MULTIMEDIA + "(" + CLAU_ID + " INTEGER PRIMARY KEY, "
			+ CLAU_NOM_FITXER + " TEXT NOT NULL, FOREIGN KEY (" + CLAU_ID + ") REFERENCES "
			+ BD_TAULA_NOTES + "(" + CLAU_ID + ") ON DELETE CASCADE);";

	// Arrays amb els camps per facilitar les consultes
	private String[] columnsNotes = new String[] { CLAU_ID, CLAU_TIPUS, CLAU_TITOL };

	private DbOpenHelper mOpenHelper;
	private SQLiteDatabase mDatabase;
	private Context mContext;

	/**
	 * El constructor crear una instancia de DbOpenHelper i li pasa el context
	 * de la activitat que la inclou.
	 * 
	 * @param context
	 */
	public DBNotes(Context context) {
		this.mContext = context;
		mOpenHelper = new DbOpenHelper(context);
	}

	/**
	 * Obre la base de dades.
	 * 
	 * @return aquest mateix objecte per poder encadenar peticions.
	 * @throws SQLException
	 *             si hi ha un error al obrir la base de dades.
	 */
	public DBNotes open() throws SQLException {
		mDatabase = mOpenHelper.getWritableDatabase();
		return this;
	}

	/**
	 * Tanca la base de dades.
	 */
	public void close() {
		try {
			mOpenHelper.close();
		} catch (SQLException e) {
			// Si hi ha un error al tancar la base de dades nom�s el mostrem al
			// log.
			Log.e(TAG, mContext.getResources().getString(R.string.error_db));
		}
	}

	/**
	 * Insireix la nota pasada com argument a la base de dades.
	 * 
	 * @param nota
	 *            Nota a inserir a la base de dades.
	 * @throws SQLException
	 *             si hi ha cap error.
	 */
	public void insertNota(Nota nota) throws SQLException {
		open();

		// Inserim les dades generals a la taula mestre
		ContentValues initialValues = new ContentValues();
		initialValues.put(CLAU_TIPUS, nota.tipus);
		initialValues.put(CLAU_TITOL, nota.titol);

		// Inserim els valors
		long id = mDatabase.insert(BD_TAULA_NOTES, null, initialValues);

		// Inserim les dades espec�fiques al tipus de nota
		switch (nota.tipus) {
		case Nota.LLOC:
			insertNotaLloc(id, nota);

		case Nota.TEXT:
			insertNotaText(id, nota);
			break;

		case Nota.IMATGE:
		case Nota.SO:
			insertNotaMultimedia(id, nota);
		}
		close();
	}

	/**
	 * Insireix la nota a la taula de llocs amb el id especificat.
	 * 
	 * @param id
	 *            id per la clau.
	 * @param nota
	 *            nota a inserir.
	 */
	private void insertNotaLloc(long id, Nota nota) {
		NotaLloc notaLloc = null;

		try {
			// Comprovem que el tipus de nota sigui correcta
			notaLloc = (NotaLloc) nota;

			// Inserim les dades
			ContentValues initialValues = new ContentValues();
			initialValues.put(CLAU_ID, id);
			initialValues.put(CLAU_LATITUD, notaLloc.latitut);
			initialValues.put(CLAU_LONGITUD, notaLloc.longitut);

			// Inserim els valors a la base de dades
			mDatabase.insert(BD_TAULA_NOTES_LLOC, null, initialValues);

		} catch (ClassCastException e) {
			Log.e(TAG, "Error al inserir les dades espec�fiques", e);
		}

	}

	/**
	 * Insireix la nota a la taula de texts amb el id especificat.
	 * 
	 * @param id
	 *            id per la clau.
	 * @param nota
	 *            nota a inserir.
	 */
	private void insertNotaText(long id, Nota nota) {
		NotaText notaText = null;

		try {
			// Comprovem que el tipus de nota sigui correcta
			notaText = (NotaText) nota;

			// Inserim les dades
			ContentValues initialValues = new ContentValues();
			initialValues.put(CLAU_ID, id);
			initialValues.put(CLAU_DESCRIPCIO, notaText.descripcio);

			// Inserim els valors a la base de dades
			mDatabase.insert(BD_TAULA_NOTES_TEXT, null, initialValues);

		} catch (ClassCastException e) {
			Log.e(TAG, "Error al inserir les dades espec�fiques", e);
		}
	}

	/**
	 * Insireix la nota a la taula de multimedia amb el id especificat.
	 * 
	 * @param id
	 *            id per la clau.
	 * @param nota
	 *            nota a inserir.
	 */
	private void insertNotaMultimedia(long id, Nota nota) {
		NotaMultimedia notaMultimedia = null;

		try {
			// Comprovem que el tipus de nota sigui correcta
			notaMultimedia = (NotaMultimedia) nota;

			// Inserim les dades
			ContentValues initialValues = new ContentValues();
			initialValues.put(CLAU_ID, id);
			initialValues.put(CLAU_NOM_FITXER, notaMultimedia.nomFitxer);

			// Inserim els valors a la base de dades
			mDatabase.insert(BD_TAULA_NOTES_MULTIMEDIA, null, initialValues);

		} catch (ClassCastException e) {
			Log.e(TAG, "Error al inserir les dades espec�fiques", e);
		}

	}

	/**
	 * Retorna un cursor amb les dades generals de la taula mestre de notes. 
	 * 
	 * @return cursor amb les dades generals de totes les notes a la primera
	 *         posici�.
	 */
	public Cursor getAllNotes() {
		open();
		Cursor cursor = mDatabase.query(BD_TAULA_NOTES, columnsNotes, null, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();
		close();
		return cursor;
	}

	/**
	 * Retorna la nota amb l'id especificat com argument.
	 * 
	 * @param id
	 *            de la nota.
	 * @return nota corresponent al id.
	 */
	public Nota getNota(int id) {
		// Recuperem les dades de la taula mestre
		Nota nota = null;

		open();
		Cursor cursor = mDatabase.query(true, BD_TAULA_NOTES, columnsNotes, CLAU_ID + " = " + id,
				null, null, null, null, null);

		if (cursor != null) {
			cursor.moveToFirst();
			nota = CursorToNota(cursor);
		}

		close();
		return nota;
	}

	/**
	 * Extreu la primera nota del cursor passat com argument.
	 * 
	 * @param cursor
	 *            cursor amb les dades.
	 * @return nota amb les dades del cursor o null si no s'ha pogut obtenir.
	 */
	private Nota CursorToNota(Cursor cursor) {
		Nota nota = null;
		int id = -1;

		// Creem una nova Nota a partir del cursor
		try {
			id = cursor.getInt(0);
			nota = new Nota(cursor.getInt(cursor.getColumnIndex(CLAU_TIPUS)),
					cursor.getString(cursor.getColumnIndex(CLAU_TITOL)));

			// Obtenim la resta de dades
			switch (nota.tipus) {
			case Nota.TEXT:
				return getNotaText(nota, id);

			case Nota.LLOC:
				return getNotaLloc(nota, id);

			case Nota.IMATGE:
			case Nota.SO:
				return getNotaMultimedia(nota, id);
			}

		} catch (Exception e) {

			Log.e(TAG, mContext.getResources().getString(R.string.error_db), e);
		}

		// Si hi ha cap error retornem null
		return null;
	}

	/**
	 * Retorna la nota de text corresponent a la id passada com argument.
	 * 
	 * @param nota
	 *            nota per completar.
	 * @param id
	 *            id de la nota.
	 * @return nota de text amb les dades obtingudes de la base de dades.
	 */
	private Nota getNotaText(Nota nota, int id) {
		NotaText novaNota = null;

		// Obtenim el cursor amb totes les dades
		Cursor cursor = getInnerJoinById(id, BD_TAULA_NOTES, BD_TAULA_NOTES_TEXT);

		// Creem una nova Nota a partir del cursor
		novaNota = new NotaText(nota.titol,
				cursor.getString(cursor.getColumnIndex(CLAU_DESCRIPCIO)));

		return novaNota;
	}

	/**
	 * Retorna la nota de lloc corresponent a la id passada com argument.
	 * 
	 * @param nota
	 *            nota per completar.
	 * @param id
	 *            id de la nota.
	 * @return nota de lloc amb les dades obtingudes de la base de dades.
	 */
	private Nota getNotaLloc(Nota nota, int id) {
		NotaLloc novaNota = null;
		// Obtenim el cursor amb totes les dades
		Cursor cursor = getInnerJoinById(id, BD_TAULA_NOTES, BD_TAULA_NOTES_LLOC,
				BD_TAULA_NOTES_TEXT);

		// Crrem una nova Nota a partir del cursor
		novaNota = new NotaLloc(nota.titol,
				cursor.getString(cursor.getColumnIndex(CLAU_DESCRIPCIO)), cursor.getDouble(cursor
						.getColumnIndex(CLAU_LATITUD)), cursor.getDouble(cursor
						.getColumnIndex(CLAU_LONGITUD)));

		return novaNota;
	}

	/**
	 * Retorna la nota de multimedia corresponent a la id passada com argument.
	 * 
	 * @param nota
	 *            nota per completar.
	 * @param id
	 *            id de la nota.
	 * @return nota de multimedia amb les dades obtingudes de la base de dades.
	 */
	private Nota getNotaMultimedia(Nota nota, int id) {
		NotaMultimedia novaNota = null;

		// Obtenim el cursor amb totes les dades
		Cursor cursor = getInnerJoinById(id, BD_TAULA_NOTES, BD_TAULA_NOTES_MULTIMEDIA);

		// Crrem una la nova Nota a partir del cursor
		novaNota = new NotaMultimedia(nota.tipus, nota.titol, cursor.getString(cursor
				.getColumnIndex(CLAU_NOM_FITXER)));
		return novaNota;
	}

	/**
	 * Aquest retorna un cursor amb el resultat de una consulta de tipus INNER
	 * JOIN de les taules passades per argument amb la id passada per argument.
	 * 
	 * @param id
	 *            id per obtenir els registres.
	 * @param taules
	 *            taules per fer l'inner join.
	 * @return cursor amb el resultat de la consulta a la primera posici�.
	 */
	private Cursor getInnerJoinById(int id, String... taules) {
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		StringBuilder setTables = new StringBuilder();
		String where = taules[0] + "." + CLAU_ID + " = " + id;
		boolean primera = true;

		for (String taula : taules) {
			if (primera) {
				// Primera taula
				setTables.append(taula);
				primera = false;
			} else {
				// La resta
				setTables.append(" INNER JOIN ");
				setTables.append(taula);
				setTables.append(" ON ");
				setTables.append(taula);
				setTables.append(".");
				setTables.append(CLAU_ID);
				setTables.append(" = ");
				setTables.append(taules[0]);
				setTables.append(".");
				setTables.append(CLAU_ID);
			}
		}

		// Afegim les taules i la clausula WHERE a la consulta
		queryBuilder.setTables(setTables.toString());
		queryBuilder.appendWhere(where);

		// Obtenim el cursor
		Cursor cursor = queryBuilder.query(mDatabase, null, null, null, null, null, null);

		if (cursor != null)
			cursor.moveToFirst();

		return cursor;
	}

	/**
	 * Aquest m�tode esborra tots els fitxers dels directores FOLDER_IMATGE i
	 * FOLDER_SO.
	 */
	private static void clearDirectories() {
		String external = Environment.getExternalStorageDirectory().getAbsolutePath();
		UtilitatsDisc.buidarDirectori(new File(external + MainActivity.FOLDER_IMATGE));
		UtilitatsDisc.buidarDirectori(new File(external + MainActivity.FOLDER_SO));
	}

	/**
	 * Elimina la nota amb la id passada per argument. Com totes les taules
	 * tenen definides la clau forana i ON DELETE CASCADE s'actualitzan totes
	 * les taules autom�ticament.
	 * 
	 * @param id
	 *            id de la nota a esborrar.
	 * @return true si s'ha esborrat o false en cas contrari.
	 */
	public boolean deleteNotaById(int id) {
		open();
		boolean correcte = mDatabase.delete(BD_TAULA_NOTES, CLAU_ID + " = " + id, null) > 0;
		close();
		return correcte;
	}
	
	/**
	 * Classe d'ajuda per gestionar la base de dades.
	 */
	private static class DbOpenHelper extends SQLiteOpenHelper {
		DbOpenHelper(Context con) {
			super(con, BD_NOM, null, VERSIO);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			try {
				// Creem les taules
				db.execSQL(BD_CREATE_NOTES);
				db.execSQL(BD_CREATE_NOTES_LLOC);
				db.execSQL(BD_CREATE_NOTES_MULTIMEDIA);
				db.execSQL(BD_CREATE_NOTES_TEXT);

				// Esborrem els arxius dels directoris
				clearDirectories();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int versioAntiga, int versioNova) {
			Log.w(TAG, "Actualitzant Base de dades de la versi� " + versioAntiga + " a "
					+ versioNova + ". Destruir� totes les dades");
			db.execSQL("DROP TABLE IF EXISTS " + BD_TAULA_NOTES);
			db.execSQL("DROP TABLE IF EXISTS " + BD_TAULA_NOTES_TEXT);
			db.execSQL("DROP TABLE IF EXISTS " + BD_TAULA_NOTES_LLOC);
			db.execSQL("DROP TABLE IF EXISTS " + BD_TAULA_NOTES_MULTIMEDIA);

			onCreate(db);
		}

		@Override
		public void onOpen(SQLiteDatabase db) {
			super.onOpen(db);
			if (!db.isReadOnly()) {
				// Habilitem l'us de claus foranes
				db.execSQL("PRAGMA foreign_keys=ON;");
			}
		}
	}
}
