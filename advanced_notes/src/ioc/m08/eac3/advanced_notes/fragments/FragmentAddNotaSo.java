package ioc.m08.eac3.advanced_notes.fragments;

import ioc.m08.eac3.advanced_notes.R;
import ioc.m08.eac3.advanced_notes.activitats.MainActivity;
import ioc.m08.eac3.advanced_notes.activitats.TemporalFileManager;
import ioc.m08.eac3.advanced_notes.dades.Nota;
import ioc.m08.eac3.advanced_notes.dades.NotaMultimedia;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.media.MediaRecorder;
import android.media.MediaRecorder.OnInfoListener;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Aquest fragment permet afegir una nota amb una gravaci� de audio feta amb el
 * dispositiu. La gravaci� s'autura autom�ticament al pausar-se el fragment o al
 * obtenir les dades de la nota.
 * 
 * @author Javier Garc�a
 * 
 */
public class FragmentAddNotaSo extends Fragment implements FragmentAddNota, OnClickListener, OnInfoListener {
	private static final String TAG = "FragmentAddNotaSo";

	// UI
	private ImageView mImageViewGravar;
	private Animation mAnimationGravar;

	// Fitxers
	private File mCurrentFile;
	private TemporalFileManager mCallback;

	// Gravacio
	private MediaRecorder mRecorder;
	private boolean mRecording = false;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// Si l'activitat no implementa la activitat de callback llencem una
		// excepci� informant
		try {
			mCallback = (TemporalFileManager) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " ha d'extendre TemporalFileManager");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_add_nota_so, container, false);

		// Inicialitzem els widgets
		mImageViewGravar = (ImageView) view.findViewById(R.id.imageViewSo);
		mImageViewGravar.setOnClickListener(this);
		mAnimationGravar = AnimationUtils.loadAnimation(this.getActivity(), R.anim.gravant);

		return view;
	}

	@Override
	public void onClick(View v) {
		if (mRecording) {
			stopRecording();
		} else {
			startRecording();
		}
	}

	/**
	 * Inicia la gravaci� de audio i l'emmagatzema al dispositu.
	 */
	private void startRecording() {
		File path = new File(Environment.getExternalStorageDirectory() + MainActivity.FOLDER_SO);
		if (!path.exists())
			path.mkdirs();

		String nomFitxerSo = path.getAbsolutePath() + "/" + System.currentTimeMillis() + ".3gp";

		// Crea el MediaRecorder i especifica la font d'�udio, el format de
		// sortida i el fitxer, i el codificador d'�udio
		mRecorder = new MediaRecorder();
		mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
		mRecorder.setOutputFile(nomFitxerSo);
		mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		mRecorder.setMaxDuration(10000);
		mRecorder.setOnInfoListener(this);

		// En enllestir la gravaci� hi pot haver problemes, per tant cal
		// preveure excepcions
		try {
			mRecorder.prepare();
			// Si s'ha pogut disposar tot correctament, es comen�a a gravar
			mRecorder.start();
			mCurrentFile = new File(nomFitxerSo);

			// Iniciem la animaci�
			mImageViewGravar.startAnimation(mAnimationGravar);
			mRecording = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Atura la gravaci� de audio.
	 */
	private void stopRecording() {
		// Aturem la animaci�
		mAnimationGravar.cancel();

		// Aturem l'enregistrament de audio
		mRecorder.stop();
		mRecorder.release();
		mRecorder = null;

		// Si s'ha creat el fitxer l'afegim a la llista d'arxius temporals
		if (mCurrentFile.exists()) {
			mCallback.addArxiusTemporals(mCurrentFile);
			Toast.makeText(getActivity(), mCurrentFile.toString(), Toast.LENGTH_SHORT).show();
		}
		mRecording = false;
	}

	@Override
	public Nota getDades(String titol) {
		// Aturem la gravacio
		if (mRecording)
			stopRecording();

		// Validem les dades
		if (titol == null || titol.length() == 0 || mCurrentFile == null) {
			return null;
		}

		// Eliminem de la llista el fitxer
		mCallback.removeArxiusTemporals(mCurrentFile);

		return new NotaMultimedia(Nota.SO, titol, mCurrentFile.getAbsolutePath());
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mRecording)
			stopRecording();
	}

	@Override
	public void onInfo(MediaRecorder mr, int what, int extra) {
		if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
			stopRecording();
		}		
	}
}
