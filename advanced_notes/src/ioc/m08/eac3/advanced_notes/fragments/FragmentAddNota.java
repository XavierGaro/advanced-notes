package ioc.m08.eac3.advanced_notes.fragments;

import ioc.m08.eac3.advanced_notes.dades.Nota;

public interface FragmentAddNota {

	/**
	 * Obt� del fragment una Nota amb el t�tol passat com argument.
	 * 
	 * @param titol
	 *            t�tol de la nota.
	 * @return nota amb les dades del fragment.
	 */
	Nota getDades(String titol);

}
