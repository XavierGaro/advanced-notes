package ioc.m08.eac3.advanced_notes.fragments;

import ioc.m08.eac3.advanced_notes.R;
import ioc.m08.eac3.advanced_notes.dades.Nota;
import ioc.m08.eac3.advanced_notes.dades.NotaText;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * Aquest fragment permet afegir una nota amb un text.
 * 
 * @author Javier Garc�a
 * 
 */
public class FragmentAddNotaText extends Fragment implements FragmentAddNota{
	private static final String TAG = "FragmentAddNotaText";
	
	private EditText mEditTextDescripcio;	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_add_nota_text, container, false);
		
		// Inicialitzem el widget
		mEditTextDescripcio = (EditText) view.findViewById(R.id.editTextDescripcio);
		
		return view;		
	}

	@Override
	public Nota getDades(String titol) {
		// Validem les dades
		String descripcio = mEditTextDescripcio.getText().toString();
		if (titol==null || titol.length()==0 || descripcio.length()==0) return null;
		
		return new NotaText(titol, descripcio);
	}
}
