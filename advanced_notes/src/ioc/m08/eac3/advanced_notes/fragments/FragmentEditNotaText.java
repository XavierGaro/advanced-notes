package ioc.m08.eac3.advanced_notes.fragments;

import ioc.m08.eac3.advanced_notes.R;
import ioc.m08.eac3.advanced_notes.dades.NotaText;
import ioc.m08.eac3.advanced_notes.database.DBNotes;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Aquest fragment perm�t visualitzar i eliminar una nota amb un text.
 * 
 * @author Javier Garc�a
 * 
 */
public class FragmentEditNotaText extends Fragment implements FragmentEditNota {
	private static final String TAG = "FragmentEditNotaText";

	// UI
	private TextView mTextViewDescripcio;
	private TextView mTextViewTitol;

	// Nota carregada
	private NotaText mNota;
	private int mId;

	// Base de dades
	private DBNotes mDatabase;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		mId = getArguments().getInt("_id");
		mDatabase = new DBNotes(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_edit_nota_text, container, false);

		mTextViewDescripcio = (TextView) view.findViewById(R.id.textViewDescripcio);
		mTextViewTitol = (TextView) view.findViewById(R.id.textViewTitol);

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		carregaNota();
	}

	/**
	 * Carrega la nota de la base de dades.
	 */
	private void carregaNota() {
		mNota = (NotaText) mDatabase.getNota(mId);

		// Afegeix la informaci� als widgets
		mTextViewDescripcio.setText(mNota.descripcio);
		mTextViewTitol.setText(mNota.titol);
	}

	@Override
	public void removeNota() {
		// No cal esborrar cap extra
	}
}
