package ioc.m08.eac3.advanced_notes.fragments;

import ioc.m08.eac3.advanced_notes.R;
import ioc.m08.eac3.advanced_notes.dades.Nota;
import ioc.m08.eac3.advanced_notes.dades.NotaLloc;
import ioc.m08.eac3.advanced_notes.location.MyLocationManager;
import ioc.m08.eac3.advanced_notes.location.OnLocationUpdateListener;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Aquest fragment permet afegir una nota amb una localitzaci�.
 * 
 * @author Javier Garc�a
 * 
 */
public class FragmentAddNotaLloc extends Fragment implements FragmentAddNota,
		OnLocationUpdateListener {
	private static final String TAG = "FragmentAddNotaLloc";

	// Mapa
	private GoogleMap mMap;
	private SupportMapFragment mMapFragment;
	private MyLocationManager mMyLocationManager;
	private Location mLocation;
	private Marker mMarker;

	// UI
	private EditText mEditTextDescripcio;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_add_nota_lloc, container, false);

		mEditTextDescripcio = (EditText) view.findViewById(R.id.editTextDescripcio);
		return view;
	}

	/**
	 * Si al restaurar el fragment no existeix el mapa l'inicialitzem. Al
	 * contrari que amb altres widgets, el mapa es un fragment niuat i no es pot
	 * inicialitzar en el m�tode onCreateView(). Tamb� ens enregistrem al
	 * LocationManager per actualitzar la localitzaci�.
	 */
	@Override
	public void onResume() {
		super.onResume();

		if (mMap == null)
			initMap();

		if (mMyLocationManager == null)
			mMyLocationManager = new MyLocationManager(this, getActivity());

		mMyLocationManager.register();
	}

	@Override
	public void onPause() {
		super.onPause();
		mMyLocationManager.unregister();
	}

	/**
	 * Inicialitza el mapa i situa el marcador a la nostra posici�.
	 */
	private void initMap() {
		mMapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager()
				.findFragmentById(R.id.map);

		// Obtenim el mapa
		mMap = mMapFragment.getMap();
	}

	/**
	 * Al crear-se la activitat comprovem si ja existeix un fragment niuat amb
	 * el mapa i si no es aix� generem una nova inst�ncia.
	 * 
	 * @param savedInstanceState
	 *            bundle
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// Obtenim un FragmentManager per fragments niuats
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

		// Cerquem el fragment de mapa
		mMapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager()
				.findFragmentById(R.id.map);

		// Si no existeix instanciem un de nou
		if (mMapFragment == null) {
			mMapFragment = SupportMapFragment.newInstance();
			fragmentManager.beginTransaction().replace(R.id.map, mMapFragment).commit();
		}
	}

	@Override
	public void onLocationUpdate(Location location) {
		if (location == null) {
			Toast.makeText(
					getActivity(),
					"No s'ha trobat cap localitzaci� anterior, s'intentar� cercar una nova localitzaci�",
					Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(getActivity(),
					"Localitzaci� actualitzada desde " + location.getProvider(), Toast.LENGTH_SHORT)
					.show();
			mLocation = location;
		}

		// Extreiem l'objecte LatLng de la localitzaci�
		LatLng posicio = new LatLng(location.getLatitude(), location.getLongitude());

		// Movem la c�mera a la posici�
		CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(posicio, 20);
		mMap.moveCamera(camera);

		// Creem les opcions del marcador
		MarkerOptions opcions = new MarkerOptions();
		opcions.position(posicio);

		// Si ja existeix un marcador l'eliminem
		if (mMarker != null) {
			mMarker.remove();
		}

		// Afegim el marcador
		mMarker = mMap.addMarker(opcions);
	}

	@Override
	public Nota getDades(String titol) {
		// Validem les dades
		String descripcio = mEditTextDescripcio.getText().toString();
		if (mLocation == null || titol == null || titol.length() == 0 || descripcio.length() == 0)
			return null;

		return new NotaLloc(titol, descripcio, mLocation.getLatitude(), mLocation.getLongitude());
	}
}
