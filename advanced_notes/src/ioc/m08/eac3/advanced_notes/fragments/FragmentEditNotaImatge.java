package ioc.m08.eac3.advanced_notes.fragments;

import ioc.m08.eac3.advanced_notes.R;
import ioc.m08.eac3.advanced_notes.dades.NotaMultimedia;
import ioc.m08.eac3.advanced_notes.database.DBNotes;

import java.io.File;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Aquest fragment perm�t visualitzar i eliminar una nota amb una imatge.
 * 
 * @author Javier Garc�a
 * 
 */
public class FragmentEditNotaImatge extends Fragment implements FragmentEditNota {
	private static final String TAG = "FragmentEditNotaImatge";

	// UI
	private ImageView mImatgeViewImatge;

	// Nota carregada
	private NotaMultimedia mNota;
	private int mId;

	// Base de dades
	private DBNotes mDatabase;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		mId = getArguments().getInt("_id");
		mDatabase = new DBNotes(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_edit_nota_imatge, container, false);

		// Incialitzem el widget
		mImatgeViewImatge = (ImageView) view.findViewById(R.id.imageViewImatge);
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		loadNota();
	}

	/**
	 * Carrega la nota de la base de dades.
	 */
	private void loadNota() {
		mNota = (NotaMultimedia) mDatabase.getNota(mId);

		// Carreguem el bitmap
		Uri uri = Uri.parse(mNota.nomFitxer);
		mImatgeViewImatge.setImageURI(uri);
	}

	@Override
	public void removeNota() {
		// Esborrar fitxer
		new File(mNota.nomFitxer).delete();		
	}
}
