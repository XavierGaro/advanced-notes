package ioc.m08.eac3.advanced_notes.fragments;

import ioc.m08.eac3.advanced_notes.R;
import ioc.m08.eac3.advanced_notes.activitats.MainActivity;
import ioc.m08.eac3.advanced_notes.activitats.TemporalFileManager;
import ioc.m08.eac3.advanced_notes.dades.Nota;
import ioc.m08.eac3.advanced_notes.dades.NotaMultimedia;

import java.io.File;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Aquest fragment permet afegir una nota amb una fotograf�a feta amb la c�mera
 * del dispositiu.
 * 
 * @author Javier Garc�a
 * 
 */
public class FragmentAddNotaImatge extends Fragment implements FragmentAddNota, OnClickListener {
	private static final String TAG = "FragmentAddNotaImatge";
	public static final int APP_CAMERA = 0;

	// UI
	private ImageView mImageViewImatge;

	// Fitxers
	private TemporalFileManager mTemporalFileManager;
	private Uri mUriImatge;
	private File mCurrentFile;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// Si l'activitat no implementa la activitat de callback llencem una
		// excepci� informant
		try {
			mTemporalFileManager = (TemporalFileManager) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " ha d'extendre TemporalFileManager");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_add_nota_imatge, container, false);

		// Inicialitzem els widgets
		mImageViewImatge = (ImageView) view.findViewById(R.id.imageViewImatge);
		mImageViewImatge.setOnClickListener(this);

		return view;
	}

	@Override
	public void onClick(View v) {
		ferFoto();
	}

	/**
	 * Llen�a una activitat per fer fotos i esperem a rebre la fotograf�a.
	 */
	private void ferFoto() {
		// Es crea l'intent per a l'aplicaci� de fotos
		Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");

		// Es crea un nou fitxer a l'emmagatzemage exter i se li passa a
		// l'intent
		File foto = new File(
				Environment.getExternalStorageDirectory() + MainActivity.FOLDER_IMATGE,
				System.currentTimeMillis() + ".jpg");
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(foto));

		// Es guarda l'identificador de la imatge per recuperar-la quan estigui
		// feta
		mUriImatge = Uri.fromFile(foto);

		// S'engega l'activitat
		startActivityForResult(intent, APP_CAMERA);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case APP_CAMERA:
			if (resultCode == Activity.RESULT_OK) {
				// El ContentResolver d�na acc�s als continguts (la imatge
				// emmagatzemada en aquest cas)
				ContentResolver contRes = getActivity().getContentResolver();

				// Cal dir-li que el contingut del fitxer ha canviat
				contRes.notifyChange(mUriImatge, null);

				// Accedeix a l'imatgeView i hi carrega la foto que ha fet la
				// c�mera
				Bitmap bitmap;

				// Com que la c�rrega de la imatge pot fallar, cal tractar les
				// possibles excepcions
				try {
					bitmap = android.provider.MediaStore.Images.Media
							.getBitmap(contRes, mUriImatge);
					mImageViewImatge.setImageBitmap(bitmap);
					Toast.makeText(getActivity(), mUriImatge.toString(), Toast.LENGTH_SHORT).show();

					// Afegim el arxiu a la llista de arxius temporals
					mCurrentFile = new File(mUriImatge.getPath());
					mTemporalFileManager.addArxiusTemporals(mCurrentFile);

				} catch (Exception e) {
					Toast.makeText(getActivity(),
							"No es pot carregar la imatge: " + mUriImatge.toString(),
							Toast.LENGTH_SHORT).show();
				}
			}
		}
	}
	
	@Override
	public Nota getDades(String titol) {
		// Validem les dades
		if (titol == null || titol.length() == 0 || mCurrentFile == null) {
			return null;
		}

		// Eliminem de la llista de fitxers temporals aquest fitxer
		mTemporalFileManager.removeArxiusTemporals(mCurrentFile);
		return new NotaMultimedia(Nota.IMATGE, titol, mCurrentFile.getAbsolutePath());
	}
}
