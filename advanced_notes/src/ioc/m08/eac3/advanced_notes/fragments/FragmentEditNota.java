package ioc.m08.eac3.advanced_notes.fragments;

public interface FragmentEditNota {

	/**
	 * Elimina els continguts extra de la nota carregada.
	 */
	void removeNota();

}
