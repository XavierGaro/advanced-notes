package ioc.m08.eac3.advanced_notes.fragments;

import ioc.m08.eac3.advanced_notes.R;
import ioc.m08.eac3.advanced_notes.dades.NotaLloc;
import ioc.m08.eac3.advanced_notes.database.DBNotes;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Aquest fragment perm�t visualitzar i eliminar una nota amb un lloc.
 * 
 * @author Javier Garc�a
 * 
 */
public class FragmentEditNotaLloc extends Fragment implements FragmentEditNota{
	private static final String TAG = "FragmentEditNotaLloc";
	
	// Nota carregada
	private NotaLloc mNota;
	private LatLng posicio;
	private int mId;

	// Mapa
	private GoogleMap mMap;
	private SupportMapFragment mMapFragment;
	private Marker mMarker;
	
	// Base de dades
	private DBNotes mDatabase;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		mId = getArguments().getInt("_id");		
		mDatabase = new DBNotes(activity);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_edit_nota_lloc, container, false);
	}

	@Override
	public void onResume() {
		super.onResume();
		loadNota();
	}
	
	/**
	 * Carrega la nota de la base de dades.
	 */
	private void loadNota() {
		mNota = (NotaLloc) mDatabase.getNota(mId);
		posicio = new LatLng(mNota.latitut, mNota.longitut);
		
		if (mMap==null) {
			mMapFragment = (SupportMapFragment) getActivity()
					.getSupportFragmentManager().findFragmentById(R.id.map);

			// Obtenim el mapa
			mMap = mMapFragment.getMap();
			
			// Movem la c�mera a la posici�
			CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(posicio, 20);
			mMap.moveCamera(camera);
			
			// Creem les opcions del marcador
			MarkerOptions opcions = new MarkerOptions();
			opcions.position(posicio);

			opcions.title(mNota.titol);
			opcions.snippet(mNota.descripcio);
			
			// Si ja existeix un marcador l'eliminem
			if (mMarker != null) {
				mMarker.remove();
			}

			// Afegim el marcador
			mMarker = mMap.addMarker(opcions);
			mMarker.showInfoWindow();
		}
	}
	
	@Override
	public void removeNota() {
		// No cal esborrar cap extra
	}
	
	/**
	 * Al crear-se la activitat comprovem si ja existeix un fragment niuat amb
	 * el mapa i si no es aix� generem una nova inst�ncia.
	 * 
	 * @param savedInstanceState
	 *            bundle
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// Obtenim un FragmentManager per fragments niuats
		FragmentManager fragmentManager = getActivity()
				.getSupportFragmentManager();

		// Cerquem el fragment de mapa
		mMapFragment = (SupportMapFragment) getActivity()
				.getSupportFragmentManager().findFragmentById(R.id.map);

		// Si no existeix instanciem un de nou
		if (mMapFragment == null) {
			mMapFragment = SupportMapFragment.newInstance();
			fragmentManager.beginTransaction().replace(R.id.map, mMapFragment)
					.commit();
		}
	}	
}
