package ioc.m08.eac3.advanced_notes.fragments;

import ioc.m08.eac3.advanced_notes.R;
import ioc.m08.eac3.advanced_notes.activitats.EditNotaActivity;
import ioc.m08.eac3.advanced_notes.dades.NotaMultimedia;
import ioc.m08.eac3.advanced_notes.database.DBNotes;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Aquest fragment perm�t visualitzar i eliminar una nota amb un s�.
 * 
 * @author Javier Garc�a
 * 
 */
public class FragmentEditNotaSo extends Fragment implements FragmentEditNota, OnClickListener,
		OnCompletionListener {
	private static final String TAG = "FragmentEditNotaSo";

	// UI
	private TextView mTextViewNomFitxer;
	private TextView mTextViewTitol;
	private ImageView mImageViewControl;

	// Nota carregada
	private NotaMultimedia mNota;
	private int mId;

	// Base de dades
	private DBNotes mDatabase;

	// Reproductor
	MediaPlayer mMediaPlayer;
	boolean playing;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		mId = getArguments().getInt("_id");
		mDatabase = new DBNotes(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_edit_nota_so, container, false);

		mTextViewTitol = (TextView) view.findViewById(R.id.textViewTitol);
		mTextViewNomFitxer = (TextView) view.findViewById(R.id.textViewNomFitxer);
		mTextViewNomFitxer.setOnClickListener(this);

		mImageViewControl = (ImageView) view.findViewById(R.id.imageButtonControl);
		mImageViewControl.setOnClickListener(this);

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		carregaNota();
	}

	/**
	 * Carrega la nota de la base de dades.
	 */
	private void carregaNota() {
		mNota = (NotaMultimedia) mDatabase.getNota(mId);

		// Afegeix la informaci� als widgets
		mTextViewNomFitxer.setText(mNota.nomFitxer);
		mTextViewTitol.setText(mNota.titol);

		// Carreguem l'arxiu
		mMediaPlayer = MediaPlayer.create(getActivity(), Uri.parse(mNota.nomFitxer));
		

		if (mMediaPlayer == null) {
			// No es pot reproduir el so, eliminem els listeners dels widgets.
			Toast.makeText(getActivity(), getResources().getString(R.string.error_fitxer),
					Toast.LENGTH_SHORT).show();
			mImageViewControl.setOnClickListener(null);
			mTextViewNomFitxer.setOnClickListener(null);

		} else {
			// Afegim la imatge i el listener per controlar la finalitzaci� de
			// la reproducci�			
			mMediaPlayer.setOnCompletionListener(this);
			mImageViewControl.setImageResource(R.drawable.ic_action_play);
		}
	}

	@Override
	public void removeNota() {
		// Aturem la reproducci� si esta reproduint
		if (playing)
			stopPlaying();

		// Esborrem el fitxer
		new File(mNota.nomFitxer).delete();
	}

	@Override
	public void onClick(View v) {
		if (playing) {
			stopPlaying();
		} else {
			startPlaying();
		}
	}

	/**
	 * Aturem la reproducci�.
	 */
	private void stopPlaying() {
		mMediaPlayer.stop();
		mImageViewControl.setImageResource(R.drawable.ic_action_play);
		playing = false;

		// Preparem el reproductor per tornar a comen�ar
		try {
			mMediaPlayer.prepare();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Inicia la reproducci�
	 */
	private void startPlaying() {
		mMediaPlayer.start();
		mImageViewControl.setImageResource(R.drawable.ic_action_stop);
		playing = true;
	}

	@Override
	public void onPause() {
		super.onPause();

		if (playing)
			stopPlaying();
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		// Al completar la reproducci� canviem la imatge per la de aturada.		
		stopPlaying();
	}
}
